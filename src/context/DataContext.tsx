import {
  Dispatch,
  PropsWithChildren,
  SetStateAction,
  createContext,
  useContext,
  useState,
} from 'react'
import useFetch from '../hooks/useFetch'

export type Pagination = {
  page: number
  pageCount: number
  pageSize: number
  total: number
}

export type SalesProps = {
  name: string
  price: number
  status: string
  payment: string
  installments: number
  createdAt: string
}

type FetchArray = {
  data: Array<{
    id: number
    attributes: SalesProps
  }>
  meta: Pagination
}

type DataContextProps = {
  loading: boolean
  error: string | null
  data: FetchArray | null
  startDate: string
  endDate: string
  setStartDate: Dispatch<SetStateAction<string>>
  setEndDate: Dispatch<SetStateAction<string>>
}

const DataContext = createContext({} as DataContextProps)

function getDate(n: number) {
  const date = new Date()
  date.setDate(date.getDate() - n)
  const dd = String(date.getDate()).padStart(2, '0')
  const mm = String(date.getMonth() + 1).padStart(2, '0')
  const yyyy = date.getFullYear()
  return `${yyyy}-${mm}-${dd}`
}

export const DataContextProvider = ({ children }: PropsWithChildren) => {
  const [startDate, setStartDate] = useState(getDate(14))
  const [endDate, setEndDate] = useState(getDate(0))

  const { data, loading, error } = useFetch<FetchArray>(
    `http://localhost:1338/api/sales`,
  )

  return (
    <DataContext.Provider
      value={{
        data,
        loading,
        error,
        startDate,
        setStartDate,
        endDate,
        setEndDate,
      }}
    >
      {children}
    </DataContext.Provider>
  )
}

export const useData = () => {
  const context = useContext(DataContext)

  if (!context) throw new Error('useData precisa estar em DataContextProvider')
  return context
}
