import { useData } from '../../context/DataContext'
import { Input } from '../Input'

export function DateRange() {
  const { startDate, setStartDate, endDate, setEndDate } = useData()

  return (
    <form className="box flex">
      <Input
        label="Início"
        value={startDate}
        onChange={({ target }) => setStartDate(target.value)}
      />
      <Input
        label="Final"
        value={endDate}
        onChange={({ target }) => setEndDate(target.value)}
      />
    </form>
  )
}
