import { CSSProperties } from 'react'
import { useData } from '../../context/DataContext'

const style: CSSProperties = {
  padding: 'var(--gap) var(--gap-s)',
  backgroundColor: 'var(--color-3)',
  border: 'none',
  borderRadius: 'var(--gap)',
  color: 'var(--color-2)',
  fontWeight: '600',
  textTransform: 'capitalize',
}

function monthName(monthNumber: number) {
  const date = new Date()
  date.setMonth(date.getMonth() + monthNumber)
  return new Intl.DateTimeFormat('pt-br', { month: 'long' }).format(date)
}

function formatDate(date: Date) {
  const dd = String(date.getDate()).padStart(2, '0')
  const mm = String(date.getMonth() + 1).padStart(2, '0')
  const yyyy = date.getFullYear()
  return `${yyyy}-${mm}-${dd}`
}

export function MonthButton({ monthNumber }: { monthNumber: number }) {
  const { setStartDate, setEndDate } = useData()

  function setMonth(monthNumber: number) {
    const date = new Date()
    date.setMonth(date.getMonth() + monthNumber)

    const firstDay = new Date(date.getFullYear(), date.getMonth(), 1)
    const lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0)
    setStartDate(formatDate(firstDay))
    setEndDate(formatDate(lastDay))
  }

  return (
    <button style={style} onClick={() => setMonth(monthNumber)}>
      {monthName(monthNumber)}
    </button>
  )
}
