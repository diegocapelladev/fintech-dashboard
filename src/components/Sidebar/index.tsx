import { NavLink } from 'react-router-dom'

import FintechSVG from '../../assets/FintechSVG'
import resume from '../../assets/icons/resumo.svg'
import sales from '../../assets/icons/vendas.svg'
import webhooks from '../../assets/icons/webhooks.svg'
import config from '../../assets/icons/configuracoes.svg'
import contact from '../../assets/icons/contato.svg'
import signOut from '../../assets/icons/sair.svg'

export function Sidebar() {
  return (
    <nav className="sidenav box bg-3">
      <FintechSVG title="Fintech Logo" />
      <ul>
        <li>
          <span>
            <img src={resume} alt="" />
          </span>
          <NavLink to="/">Resumo</NavLink>
        </li>

        <li>
          <span>
            <img src={sales} alt="" />
          </span>
          <NavLink to="/vendas">Vendas</NavLink>
        </li>

        <li>
          <span>
            <img src={webhooks} alt="" />
          </span>
          <a>Webhooks</a>
        </li>

        <li>
          <span>
            <img src={config} alt="" />
          </span>
          <a>Configurações</a>
        </li>

        <li>
          <span>
            <img src={contact} alt="" />
          </span>
          <a>Contato</a>
        </li>

        <li>
          <span>
            <img src={signOut} alt="" />
          </span>
          <a>Sair</a>
        </li>
      </ul>
    </nav>
  )
}
