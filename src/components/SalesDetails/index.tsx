import { NavLink } from 'react-router-dom'
import { SalesProps } from '../../context/DataContext'

type Props = {
  sales: {
    id: number
    attributes: SalesProps
  }
}

export function SalesDetails({ sales }: Props) {
  return (
    <div className="venda box">
      <NavLink to={`/vendas/${sales.id}`} style={{ fontFamily: 'monospace' }}>
        {sales.id}
      </NavLink>
      <div>{sales.attributes.name}</div>
      <div>
        {sales.attributes.price.toLocaleString('pt-br', {
          style: 'currency',
          currency: 'BRL',
        })}
      </div>
    </div>
  )
}
