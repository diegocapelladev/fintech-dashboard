import {
  Legend,
  Line,
  LineChart,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  YAxis,
} from 'recharts'
import { SalesProps } from '../../context/DataContext'

type SalesOfTheDay = {
  createdAt: string
  pago: number
  processando: number
  falha: number
}

type SalesGraphProps = {
  salesData: Array<{
    id: number
    attributes: SalesProps
  }>
}

type FormatDateProps = {
  id: number
  attributes: SalesProps
}

function transformData(data: FormatDateProps[]) {
  const days = data.reduce((acc: { [key: string]: SalesOfTheDay }, item) => {
    const day = item.attributes.createdAt.split(' ')[0]
    if (!acc[day]) {
      acc[day] = {
        createdAt: day,
        pago: 0,
        processando: 0,
        falha: 0,
      }
    }
    acc[day][item.attributes.status] += item.attributes.price
    return acc
  }, {})

  return Object.values(days).map((day) => ({
    ...day,
    createdAt: day.createdAt.substring(5),
  }))
}

export function SalesGraph({ salesData }: SalesGraphProps) {
  const transformedData = transformData(salesData)

  return (
    <ResponsiveContainer width="99%" height={400}>
      <LineChart data={transformedData}>
        <XAxis dataKey="data" />
        <YAxis />
        <Tooltip />
        <Legend />
        <Line type="monotone" dataKey="pago" stroke="#A36AF9" strokeWidth={3} />
        <Line
          type="monotone"
          dataKey="processando"
          stroke="#FBCB21"
          strokeWidth={3}
        />
        <Line
          type="monotone"
          dataKey="falha"
          stroke="#000000"
          strokeWidth={3}
        />
      </LineChart>
    </ResponsiveContainer>
  )
}
