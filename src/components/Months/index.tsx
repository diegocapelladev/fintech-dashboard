import { MonthButton } from '../MonthButton'

export function Months() {
  return (
    <div className="flex">
      <MonthButton monthNumber={-3} />
      <MonthButton monthNumber={-2} />
      <MonthButton monthNumber={-1} />
      <MonthButton monthNumber={0} />
    </div>
  )
}
