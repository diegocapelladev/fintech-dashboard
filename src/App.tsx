import { BrowserRouter, Route, Routes } from 'react-router-dom'
import { Header } from './components/Header'
import { Sidebar } from './components/Sidebar'
import { Resume } from './pages/Resume'
import './style.css'
import { Sales } from './pages/Sales'
import { SalesItem } from './pages/SaleItem'

function App() {
  return (
    <BrowserRouter>
      <div className="container">
        <aside>
          <Sidebar />
        </aside>
        <main>
          <Header />
          <Routes>
            <Route path="/" element={<Resume />} />
            <Route path="/vendas" element={<Sales />} />
            <Route path="/vendas/:id" element={<SalesItem />} />
          </Routes>
        </main>
      </div>
    </BrowserRouter>
  )
}

export default App
