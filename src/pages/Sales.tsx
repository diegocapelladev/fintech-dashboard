import { SalesDetails } from '../components/SalesDetails'
import { useData } from '../context/DataContext'

export function Sales() {
  const { data: sales } = useData()

  if (sales === null) return null

  return (
    <ul>
      {sales.data.map((item) => (
        <li key={item.id}>
          <SalesDetails sales={item} />
        </li>
      ))}
    </ul>
  )
}
