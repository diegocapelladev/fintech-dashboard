import { SalesGraph } from '../components/SalesGraph'
import { useData } from '../context/DataContext'

export function Resume() {
  const { data: sales } = useData()

  if (sales === null) return null

  return (
    <section>
      <div className="resumo flex mb">
        <div className="box">
          <h2>Vendas</h2>
          <span>
            {sales.data
              .filter((i) => i.attributes.status !== 'falha')
              .reduce((acc, item) => acc + item.attributes.price, 0)
              .toLocaleString('pt-br', { style: 'currency', currency: 'BRL' })}
          </span>
        </div>

        <div className="box">
          <h2>Recebido</h2>
          <span>
            {sales.data
              .filter((i) => i.attributes.status === 'pago')
              .reduce((acc, item) => acc + item.attributes.price, 0)
              .toLocaleString('pt-br', { style: 'currency', currency: 'BRL' })}
          </span>
        </div>

        <div className="box">
          <h2>Processando</h2>
          <span>
            {sales.data
              .filter((i) => i.attributes.status === 'processando')
              .reduce((acc, item) => acc + item.attributes.price, 0)
              .toLocaleString('pt-br', { style: 'currency', currency: 'BRL' })}
          </span>
        </div>
      </div>
      <div className="box mb">
        <SalesGraph salesData={sales.data} />
      </div>
    </section>
  )
}
