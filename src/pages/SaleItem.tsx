import { useParams } from 'react-router-dom'
import useFetch from '../hooks/useFetch'
import { Pagination, SalesProps } from '../context/DataContext'
import { Loading } from '../components/Loading'

type FetchItemProps = {
  data: {
    id: number
    attributes: SalesProps
  }
  meta: Pagination
}

export function SalesItem() {
  const { id } = useParams()

  const { data: sales, loading } = useFetch<FetchItemProps>(
    `http://localhost:1338/api/sales/${id}`,
  )

  if (loading === true) return <Loading />

  if (sales === null) return null

  return (
    <div>
      <div className="box mb">ID: {sales.data.id}</div>
      <div className="box mb">Nome: {sales.data.attributes.name}</div>
      <div className="box mb">
        Preço:{' '}
        {sales.data.attributes.price.toLocaleString('pt-br', {
          style: 'currency',
          currency: 'BRL',
        })}
      </div>
      <div className="box mb">Status: {sales.data.attributes.status}</div>
      <div className="box mb">Pagamento: {sales.data.attributes.payment}</div>
    </div>
  )
}
