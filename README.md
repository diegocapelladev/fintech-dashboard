# React + TypeScript + Vite

## Run

`npm install`

`npm run dev`

## Local

[localhost:3000](http://localhost:3000/)

## Preview

<img src="./src/assets/preview/preview-1.png" maxWidth="800px" />

<img src="./src/assets/preview/preview-2.png" maxWidth="800px" />

#### *Obs: Create your own database*